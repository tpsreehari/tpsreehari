# Sreehari T P

[![Hey! Everyone](https://tpsreehari.thequestionmark.co.in/wp-content/uploads/2023/05/Sreehari_TP_pic-1-300x300.jpg)](https://tpsreehari.thequestionmark.co.in/)

**Hey, There!**

**I’m Sreehari, and I’m an IT professional who absolutely loves learning and experimenting, even if it means making a few blunders along the way.**

## Write-Ups and Tutorials

Also, don't forget to check out my written tutorials and documentation. These resources serve as valuable references that complement the information in my videos. You'll find helpful example code snippets, templates, and more to support your learning process.

- [Groundwork](https://gitlab.com/tpsreehari/groundwork) - Mature casts out of the foundry. Templates for various projects like Docker, K8S, Ansible, etc.
- [Playground](https://gitlab.com/tpsreehari/playground) - My personal foundry where i get my hands dirty. It may have some broken code as well.
- [Cheat-Sheets](https://gitlab.com/tpsreehari/cheat-sheets) - Command Reference for various tools and technologies.
- [Homelab](https://gitlab.com/tpsreehari/homelab) - This is my Homelab documentation, and configurations for infrastructure, applications, networking, and more.

## Let's make a difference together


[https://www.linkedin.com/in/tpsreehari](https://www.linkedin.com/in/tpsreehari)